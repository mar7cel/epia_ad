﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class ad_practica1 : MonoBehaviour {
	

	// Use this for initialization
	void Start () {

		string file = Application.persistentDataPath + "/sample.txt";
		Debug.Log ("File to use: " + file);

		string file2 = Application.persistentDataPath + "/sample2.txt";

		FileStream fs1 = new FileStream (file, FileMode.Open);
		FileStream fs2 = new FileStream (file2, FileMode.Create);

		BinaryReader br = new BinaryReader (fs1);
		BinaryWriter bw = new BinaryWriter (fs2);

		byte tmp;
		while (br.PeekChar() != -1) {
			tmp = br.ReadByte();
			bw.Write (tmp);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
